import { combineReducers } from 'redux';
import { SET_TOPLIST } from './PodcastActions';

const INITIAL_STATE = {
    isDBReady: false,
    toplist: [],
    podcasts: {},
};

const podcastsReducer = (state = INITIAL_STATE, action) => {
    console.log(action.type);
    switch (action.type) {
        case 'DB_READY':
            return { ...state, isDBReady: true };
        case 'SET_TOPLIST':
            return { ...state, toplist: action.payload };
        case 'SET_PODCAST':
            return { ...state, podcasts: { ...state.podcasts, [action.id]: action.payload } };
        default:
            return state;
    }
};

export default combineReducers({
    podcasts: podcastsReducer,
});