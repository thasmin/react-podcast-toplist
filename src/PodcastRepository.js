'use strict';

import SQLite from 'react-native-sqlite-storage';
import * as iTunes from './ITunes';
import { dbReady, setToplist, setPodcast } from './PodcastActions';

let db = null;
let store = null;

export const PodcastRepository = {
    loadDB: function(reduxStore) {
        //SQLite.deleteDatabase({name: 'podcasts.db'});

        store = reduxStore;
        db = SQLite.openDatabase({name: 'podcasts.db'}, db => {
            db.transaction(tx => {
                tx.executeSql("CREATE TABLE IF NOT EXISTS toplists(id INTEGER PRIMARY KEY AUTOINCREMENT, category TEXT, date TEXT);")
                tx.executeSql("CREATE UNIQUE INDEX IF NOT EXISTS toplist_category_date_idx ON toplists(category, date);")
                tx.executeSql("CREATE TABLE IF NOT EXISTS toplist_items(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, artworkUrl100 TEXT, itunes_id TEXT, toplist_id INTEGER, FOREIGN KEY(toplist_id) REFERENCES toplists(id));")
                tx.executeSql("CREATE TABLE IF NOT EXISTS podcasts(id integer PRIMARY KEY, rss_url TEXT, title TEXT, description TEXT, lastUpdate TEXT);")
                tx.executeSql("CREATE TABLE IF NOT EXISTS episodes(id integer PRIMARY KEY AUTOINCREMENT, title TEXT, published TEXT, podcast_id TEXT, FOREIGN KEY(podcast_id) REFERENCES podcasts(id));")
                console.log('created tables');
                store.dispatch(dbReady());
            });
        });
    },
    getToplist: function(category, date) {
        db.transaction(tx => {
            tx.executeSql(
                "SELECT item.* FROM toplists t JOIN toplist_items item ON t.id = item.toplist_id WHERE t.category = ? AND t.date = ?",
                [category, date],
                (_, res) => { 
                    if (res.rows.length > 0) {
                        console.log('using toplist from db');
                        return store.dispatch(setToplist(res.rows.raw()));
                    }
                    iTunes.loadToplist().then((podcasts) => {
                        db.transaction(tx2 => {
                            tx2.executeSql("DELETE FROM toplist_items WHERE toplist_id = (SELECT id FROM toplists WHERE category = ? AND date = ?)", [category, date]);
                            tx2.executeSql("INSERT OR IGNORE INTO toplists(category, date) VALUES(?, ?)", [category, date]);
                        });
                        db.transaction(tx2 => {
                            tx2.executeSql("SELECT id FROM toplists WHERE category = ? AND date = ?", [category, date], (tx3, res2) => {
                                const toplistId = res2.rows.item(0).id;
                                const insertSql = "INSERT INTO toplist_items(toplist_id, name, artworkUrl100, itunes_id) VALUES(?, ?, ?, ?)";
                                for (const p of podcasts)
                                    tx3.executeSql(insertSql, [toplistId, p.name, p.artworkUrl100, p.id])
                                store.dispatch(setToplist(podcasts));
                            });
                        });
                    });
                 }
            )
        })
    },
    refreshPodcast: function(iTunesId) {
        iTunes.loadPodcast(iTunesId).then(podcast => {
            db.executeSql("INSERT OR IGNORE INTO podcasts(id) VALUES(?)", [ iTunesId ]);
            db.executeSql("SELECT * FROM podcasts WHERE id = ?", [ iTunesId ], res => {
                const podcastId = res.rows.item(0).id;
                db.transaction(tx => {
                    tx.executeSql(
                        "UPDATE podcasts SET lastUpdate=datetime('now'), title=?, description=?, rss_url=? WHERE id=?",
                        [ podcast.title, podcast.description, podcast.rssUrl, podcastId ]
                    );
                    tx.executeSql("DELETE FROM episodes WHERE podcast_id = ?", [ podcastId ]);
                    const insertSql = "INSERT INTO episodes(podcast_id, title, published) VALUES(?, ?, ?)";
                    for (let e of podcast.items)
                        tx.executeSql(insertSql, [ podcastId, e.title, e.published ]);
                });
            });
            store.dispatch(setPodcast(iTunesId, podcast));
            console.log('saved podcast ' + iTunesId);
        });
    },
    getPodcast: function(iTunesId) {
        db.executeSql("SELECT * FROM podcasts WHERE id = ?", [ iTunesId ],
            res => {
                if (res.rows.length == 0)
                    return this.refreshPodcast(iTunesId);
                const podcast = res.rows.item(0);
                const lastUpdate = new Date(podcast.lastUpdate);
                const oneHourAgo = new Date() - (1000 * 60 * 60);
                if (lastUpdate < oneHourAgo)
                    return this.refreshPodcast(iTunesId);
                
                // attach episodes to podcast
                db.executeSql("SELECT * FROM episodes WHERE podcast_id = ?", [ podcast.id ], 
                res2 => {
                    podcast.items = res2.rows.raw();
                    store.dispatch(setPodcast(iTunesId, podcast));
                })
            }
        );
    },
}

function getToday() {
    return new Date().toJSON().substr(0, 10);
}

export const PodcastMiddleware = store => next => action => {
    if (action.type == 'LOAD_TOPLIST') {
        next(action);
        if (!store.getState().podcasts.isDBReady) {
            console.log('waiting for db');
            return setTimeout(() => store.dispatch(action), 10);
        }
        PodcastRepository.getToplist('All', getToday());
    } else if (action.type == 'LOAD_PODCAST') {
        next(action);
        const iTunesId = action.payload;
        PodcastRepository.getPodcast(iTunesId);
    } else {
        next(action);
    }
}
