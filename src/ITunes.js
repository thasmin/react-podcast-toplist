'use strict';

import * as rssParser from 'react-native-rss-parser';

export function loadToplist() {
    const iTunesUrl = "https://rss.itunes.apple.com/api/v1/us/podcasts/top-podcasts/all/25/explicit.json"
    return new Promise((resolve, reject) => {
        fetch(iTunesUrl)
            .then(response => response.json())
            .then(json => {
                resolve(json.feed.results);
            })
            .catch(reject);
    });
}

export function loadPodcast(iTunesId) {
    return new Promise((resolve, reject) => {
        const url = 'https://itunes.apple.com/lookup?id=' + iTunesId + '&entity=podcast';
        fetch(url)
            .then(response => response.json())
            .then(json => {
                const rssUrl = json.results[0].feedUrl;
                fetch(rssUrl)
                    .then(response => response.text())
                    .then(text => rssParser.parse(text))
                    .then(rss => { resolve({ rssUrl: rssUrl, ...rss }); })
            })
            .catch(reject);
    });
}
