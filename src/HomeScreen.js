'use strict';

import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { connect, bindActionCreators } from 'react-redux';

import styles from './Styles';

const { height, width } = Dimensions.get('window');
const rows = 3;
const itemWidth = width / rows;

const localStyles = StyleSheet.create({
  itemStyle: {
    width: itemWidth,
    alignItems: 'center',
  },
  itemPic: {
    width: itemWidth,
    height: itemWidth
  },
  itemTitle: {
    textAlign: 'center',
    display: 'none'
  }
});

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigate, navigation }) => ({
    title: 'Top Audio Podcasts',
    headerLeft: <Icon name="menu" size={24}
      color="#ffffff"
      style={styles.drawerIcon}
      onPress={() => navigation.openDrawer()} />,
  });

  componentWillMount() {
    this.props.loadToplist();
  }

  render() {
    if (this.props.podcasts.toplist.length == 0) {
      return (
        <View style={styles.container}>
          <Text>Loading...</Text>
          <ActivityIndicator size="large" color="#f4511e" />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={ this.props.podcasts.toplist }
          keyExtractor={ item => item.id }
          numColumns={rows}
          renderItem={({item}) => this._renderItem(item)}
        />
      </View>
    )
  }

  _onItemPress(toplist_item) {
    this.props.navigation.navigate('details', { toplist_item });
  }

  _renderItem(toplist_item) {
    return (
      <TouchableOpacity onPress={() => { this._onItemPress(toplist_item) }}>
        <View style={localStyles.itemStyle}>
          <Image
            style={localStyles.itemPic}
            source={{ uri: toplist_item.artworkUrl100 }}
          />
          <Text style={localStyles.itemTitle}>{toplist_item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = (state) => {
  const { podcasts } = state
  return { podcasts }
};

import { loadToplist } from "./PodcastActions";
const mapDispatchToProps = dispatch => ({
  loadToplist: () => dispatch(loadToplist())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);