export const dbReady = () => (
    { type: 'DB_READY' }
);

export const loadToplist = () => (
    { type: 'LOAD_TOPLIST' }
);

export const setToplist = toplist => (
    {
        type: 'SET_TOPLIST',
        payload: toplist,
    }
);

export const loadPodcast = id => (
    {
        type: 'LOAD_PODCAST',
        payload: id,
    }
);

export const setPodcast = (id, podcast) => (
    {
        type: 'SET_PODCAST',
        id: id,
        payload: podcast,
    }
);