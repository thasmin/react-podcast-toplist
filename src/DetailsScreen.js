'use strict';

import React from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { connect } from 'react-redux';

import styles from './Styles';

class DetailsScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const toplist_item = navigation.state.params.toplist_item;
    return {
      title: toplist_item.name,
      headerLeft: <Icon name="menu" size={24}
        color="#ffffff"
        style={styles.drawerIcon}
        onPress={() => navigation.openDrawer()} />
    };
  };

  componentWillMount() {
    const toplist_item = this.props.navigation.state.params.toplist_item;
    this.props.loadPodcast(toplist_item.itunes_id);
  }

  render() {
    const toplist_item = this.props.navigation.state.params.toplist_item;

    if (!(toplist_item.itunes_id in this.props.podcasts.podcasts)) {
      return (
        <View style={styles.container}>
          <Text>Loading...</Text>
          <ActivityIndicator size="large" color="#f4511e" />
        </View>
      )
    }

    const details = this.props.podcasts.podcasts[toplist_item.itunes_id];
    return (
      <View>
        <View style={{flexDirection:'row'}}>
            <Image
              style={{ flexBasis: 170, width: 170, height: 170, marginRight: 10 }}
              source={{ uri: toplist_item.artworkUrl100 }}
            />
            <View style={{flexGrow:1, flexShrink:1, alignItems:'center'}}>
              <Text style={{fontSize:24}}>{details.title}</Text>
              <Text style={{maxHeight: 120, marginBottom:5}}>{details.description}</Text>
            </View>
        </View>
        <Text style={{fontSize:18, textAlign:'center'}}>Episodes</Text>
        <FlatList
          data={details.items}
          keyExtractor={ (item, index) => ''+index }
          renderItem={({item}) => this._renderEpisode(item)}
        />
      </View>
    )
  }

  _renderEpisode(episode) {
    return (
      <View style={[styles.cardContainer, styles.card]}>
        <Text>{episode.title}</Text>
        <Text>{episode.pubDate}</Text>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { podcasts } = state
  return { podcasts }
};

import { loadPodcast } from "./PodcastActions";
const mapDispatchToProps = dispatch => ({
  loadPodcast: (id) => dispatch(loadPodcast(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);