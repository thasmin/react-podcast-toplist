'use strict';

import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { createAppContainer, createDrawerNavigator, createStackNavigator } from 'react-navigation';

// UI
import styles from './Styles';
import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import podcastsReducer from './PodcastReducer';
import { PodcastRepository, PodcastMiddleware } from './PodcastRepository';
const store = createStore(
  podcastsReducer,
  applyMiddleware(PodcastMiddleware)
);
PodcastRepository.loadDB(store);

const defaultNavigationOptions = ({navigate, navigation}) => ({
  headerStyle: { backgroundColor: '#f4511e' },
  headerTintColor: '#fff',
});

class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigate, navigation }) => ({
    title: 'Settings',
    headerLeft: <Icon name="menu" size={24}
      color="#ffffff"
      style={styles.drawerIcon}
      onPress={() => navigation.openDrawer()} />,
  });

  render() {
    return (
      <View style={styles.container}>
        <Text>No settings yet!</Text>
      </View>
    );
  }
}

const SettingsNav = createStackNavigator(
  {
    settings: { screen: SettingsScreen },
  },
  {
    defaultNavigationOptions
  }
);

const HomeNavigation = createStackNavigator(
  {
    home: { screen: HomeScreen },
    details: { screen: DetailsScreen }
  },
  {
    defaultNavigationOptions
  }
);

const DrawerNavScreen = createDrawerNavigator(
  {
    home: {
      screen: HomeNavigation,
      navigationOptions: {
        drawerLabel: 'Home'
      },
    },
    stacknav: {
      screen: SettingsNav,
      navigationOptions: {
        drawerLabel: 'Settings'
      }
    },
  }
)

const DrawerNav = createAppContainer(DrawerNavScreen);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <DrawerNav />
      </Provider>
    );
  }
}

